package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//si no le indico nada le estoy diciendo que tome el nombre de la classe Paciente
//from Paciente---nombre de la clase el cual le indico que es una entidad
@Table(name="persona")
public class Persona {

	@Id
	//el identity en posgre se comporta como "el mismo motor genera una secuencia"
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int idPersona;
    //@column me permite definir nombre,longitud del elemento
	@Column(name="nombres", nullable=true, length=30)
	private String nombres;
	
	@Column(name="apellidos", nullable=true, length=30)
    private String apellidos;

	public int getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	
 

}
