package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//si no le indico nada le estoy diciendo que tome el nombre de la classe Paciente
//from Paciente---nombre de la clase el cual le indico que es una entidad
@Table(name="producto")
public class Producto {

	
	@Id
	//el identity en posgre se comporta como "el mismo motor genera una secuencia"
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int idProducto;
    //@column me permite definir nombre,longitud del elemento
	@Column(name="nombres", nullable=true, length=30)
	private String nombres;
	
	@Column(name="marca", nullable=true, length=30)
    private String marca;

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

}
